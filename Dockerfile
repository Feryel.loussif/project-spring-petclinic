FROM openjdk:8u111-jdk-alpine
EXPOSE 8080 
VOLUME /tmp
ADD /target/*.jar app.jar
ENTRYPOINT ["java","-Djava.security.edg=file:/dev/./urandom","-jar","/app.jar"]
